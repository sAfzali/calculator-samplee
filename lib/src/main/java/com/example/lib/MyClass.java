package com.example.lib;

import java.util.Scanner;

public class MyClass {
    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        print("Enter First Number");
        double firstNumber = scanner.nextDouble();
        print("Enter Action");
        String action = scanner.next();
        print("Enter Second Number");
        double secondNumber = scanner.nextDouble();
        print(calculate(firstNumber, secondNumber, action));


    }

    private static double calculate(double first, double second, String action) {
        double result = 0;
        switch (action) {
            case "+":
                result = first + second;
                break;
            case "-":
                result = first - second;
                break;
            case "/":
                result = first / second;
                break;
            case "*":
                result = first * second;
                break;
        }
        return result;
    }


    private static void print(String msg) {
        System.out.println(msg);
    }

    private static void print(double msg) {
        System.out.println(msg);
    }
}
